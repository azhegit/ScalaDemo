package azhe.spark.sql

import azhe.spark.SparkHelper
import org.apache.spark.rdd.RDD

/**
  * 计算出现次数排前3的词
  * Created by azhe on 2019-07-15 16:44
  */
object SQLWordCount {
  def main(args: Array[String]): Unit = {
    val session = SparkHelper.sparkSession
    val words: RDD[Words] = session.sparkContext.textFile("data/wc.txt").flatMap(_.split(" ")).map(word => Words(word.trim, 1))

    val sqlContext = session.sqlContext
    val wordsFrame = sqlContext.createDataFrame(words)

    wordsFrame.createTempView("words")

    sqlContext.sql("select word,count(*) as count from words group by word order by count desc ").show(3)

  }
}

case class Words(word: String, count: Int)
