package azhe.spark.mllib

import azhe.spark.SparkHelper
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.stat.{MultivariateStatisticalSummary, Statistics}

/**
  * Created by azhe on 2019-07-15 19:08
  */
object MLlibDemo {
  def main(args: Array[String]): Unit = {

    val observations = SparkHelper.sparkSession.sparkContext.parallelize(
      Seq(
        Vectors.dense(1.0, 10.0, 100.0),
        Vectors.dense(2.0, 20.0, 200.0),
        Vectors.dense(3.0, 30.0, 300.0)
      )
    )

    // 统计
    val summary: MultivariateStatisticalSummary = Statistics.colStats(observations)
    //    最大值
    println(summary.max)
    //    均值
    println(summary.mean)
    //    方差
    println(summary.variance)
  }
}
