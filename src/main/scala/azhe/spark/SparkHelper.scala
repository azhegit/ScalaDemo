package azhe.spark

import org.apache.spark.sql.SparkSession

/**
  * Created by azhe on 2019-07-15 16:12
  */
object SparkHelper {

  lazy val sparkSession = SparkSession
    .builder()
    .master("local[4]")
    .appName("master")
    .getOrCreate()

}
