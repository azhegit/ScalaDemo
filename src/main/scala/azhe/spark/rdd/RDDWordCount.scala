package azhe.spark.rdd

import azhe.spark.SparkHelper

/**
  * 计算出现次数排前3的词
  * Created by azhe on 2019-07-15 15:59
  */
object RDDWordCount {

  def main(args: Array[String]): Unit = {

    val word = SparkHelper.sparkSession.sparkContext.textFile("data/wc.txt").flatMap(_.split(" ")).map(word => (word.trim, 1)).reduceByKey(_ + _).sortBy(_._2, false)

    word.take(3).foreach(println)

  }
}
