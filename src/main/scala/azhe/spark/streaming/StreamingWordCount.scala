package azhe.spark.streaming

import azhe.spark.SparkHelper
import org.apache.spark.streaming.{Seconds, StreamingContext}

/**
  * 计算5s窗口内的单词统计
  *
  * 先启动socket server，2种发送数据方式:
  *   1. 本机启动端口：nc -l 10001
  *   2. 启动azhe.spark.streaming.RandomServer程序
  * 再启动本程序
  *
  * Created by azhe on 2019-07-15 17:26
  */
object StreamingWordCount {

  def main(args: Array[String]): Unit = {

    val ssc: StreamingContext = new StreamingContext(SparkHelper.sparkSession.sparkContext, Seconds(5))

    val words = ssc.socketTextStream("localhost", 10001)

    val pairs = words.map((_, 1)).reduceByKey(_ + _)

    pairs.print()
    ssc.start()
    ssc.awaitTermination()
  }
}
