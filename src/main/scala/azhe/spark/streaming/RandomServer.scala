package azhe.spark.streaming

import java.io.DataOutputStream
import java.net.{ServerSocket, Socket}

/**
  * socket server端，随机发送字母
  * Created by azhe on 2019-07-15 17:31
  */
object RandomServer {

  def main(args: Array[String]): Unit = {
    //创建服务端
    val listener: ServerSocket = new ServerSocket(10001)
    println("服务端正在服务中.......")
    while (true) {
      val words: Array[String] = Array("a", "b", "c", "d", "e", "f", "g")


      val word = words((math.random * (words.length)).toInt)
      println(word)
      Thread.sleep(1000)

      //建立socket通信
      val socket: Socket = listener.accept()
      //创建输入输出流
      val out = new DataOutputStream(socket.getOutputStream())
      out.write(word.getBytes("utf-8"))
      out.write("\n".getBytes("utf-8"))
      out.flush()
      out.close()
      socket.close()

    }
  }
}
