package azhe.scala

/**
  * Created by azhe on 2019-07-11 21:43
  */
/**
  * 普通类
  * @param name
  * @param age
  */
class ClassDemo(name:String,age:Int) {
  override def toString = s"I`m $name,$age years old！"
}

/**
  * 抽象类
  * 它定义了一些方法但没有实现他们。取而代之的是有扩展抽象类的子类定义这些方法。不能创建抽象类的实例
  */
abstract class AbstractDemo{
  def say(word:String)
}
class AbstractDemo_1 extends AbstractDemo{
  override def say(word: String) = println(s"hi,$word")
}

/**
  * 样例类
  * @param name
  * @param age
  */
case class ClassDemo3(name:String,age:Int)


//特征，类似java中的interface
trait Name {
  val name: String
}
trait Age {
  val age: Int
}
class Person(name1:String) extends Name {
  override val name = name1
  val hobby="swim"

  override def toString = s"I`m $name , I like $hobby"
}

class Person2(name1:String,age1:Int) extends Name with Age {
  override val name = name1
  override val age = age1
  val hair="black"

  override def toString = s"I`m $name ,  $age years old, my hair is $hair"
}

object ClassTest{

  private val person = new ClassDemo("Lily",18)

  def main(args: Array[String]): Unit = {
    println(person)

    val demo2 = new AbstractDemo_1
    demo2.say("scala")

    val lilei = ClassDemo3("Lilei",20)
    println(lilei)


    val persona = new Person("张三")
    val personb = new Person2("李四",26)

    println(persona)
    println(personb)


  }
}



