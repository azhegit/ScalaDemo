package azhe.scala

import java.io.{FileNotFoundException, FileReader, IOException}

/**
  * Created by azhe on 2019-07-11 21:27
  */
object ExceptionDemo {
  def main(args: Array[String]) {
    try {
      val f = new FileReader("input.txt")
    } catch {
      case ex: FileNotFoundException => {
        println("Missing file exception")
      }
      case ex: IOException => {
        println("IO Exception")
      }
    } finally {
      println("Exiting finally...")
    }
  }
}

//Missing file exception
//Exiting finally...
