package azhe.scala

import scala.util.Random

/**
  * Created by azhe on 2019-07-11 17:58
  */
object BlockDemo {

  //  方法和变量也可以在用大括号{}表示的代码块中定义。
  val name = {
    "Lily"
  }
  val age = 18
  val province, city = "北京"

  /*
  代码块的结果是在代码块中计算的最后一行，如以下示例所示。
  变量定义也可以是代码块。
  */
  var sex = {
    val flag = Random.nextInt(2)
    if (flag == 1) "男" else "女"
  }


  //  字符串插值是一种将字符串中的值与变量组合的机制。
  def introduction() = println(s"Hello,I`m $name, sex is $sex ,from $province,$city 市 ")

  def main(args: Array[String]): Unit = {
    introduction()
  }
}
