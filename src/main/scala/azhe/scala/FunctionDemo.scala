package azhe.scala

/**
  * Created by azhe on 2019-07-11 21:33
  */
object FunctionDemo {

  //  匿名函数
  val sum = (x: Int, y: Int) => x + y

  def main(args: Array[String]): Unit = {

    val sumResult = sum(1, 3)
    println(sumResult)
  }
}
