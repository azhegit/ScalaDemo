package azhe.scala

/**
  * Created by azhe on 2019-07-11 21:14
  */
object ChooseDemo {
  def main(args: Array[String]): Unit = {

    //if else
    val a = 1
    val b = 2
    val min = if (a < b) a else b
    println(s"min : $min") //1

    //match
    val english = a match {
      case 1 => "one"
      case 2 => "two"
      case _ => "sorry, I don`t know"
    }
    println(s"english : $english") // one

    val elem: Any = 1.1
    val x = elem match {
      case _: Int => s"elem: $elem 是一个数字"
      case _: Double => s"elem: $elem 是一个浮点型数值"
      case i: String => s"elem: $i 是一个字符串"
      case "spark" => s"elem: $elem 是分布式计算引擎"
    }
    println(s"$x")

  }
}
